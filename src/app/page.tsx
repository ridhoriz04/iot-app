import Image from "next/image";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-center bg-black">
      <div>
        <h1 className="text-3xl font-bold text-teal-600 animate-pulse">
          Hello /
        </h1>
      </div>
    </main>
  );
}
